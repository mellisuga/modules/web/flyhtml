
const XHR = require("core/utils/xhr_async");

module.exports = class {
  constructor(req_path, element, form_object, callback, cfg) {
    this.element = element;
    

    this.value_elements = [
      ...element.querySelectorAll("input[type=text]"),
      ...element.querySelectorAll("input[type=password]"),
      ...element.querySelectorAll("input[type=number]"),
      ...element.querySelectorAll("input[type=date]"),
      ...element.querySelectorAll("textarea"),
      ...element.querySelectorAll("select")
    ];


    this.submit_element = element.querySelector("input[type=submit]");

    this.fform_element = element.querySelector("form");

    let _this = this;

    this.submit_element.addEventListener("click", async function(ev) {
      try {
        if (!form_object) form_object = {};
        for (let i = 0; i < _this.value_elements.length; i++) {
          let input_element = _this.value_elements[i];
          console.log(input_element);
          form_object[input_element.name] = input_element.value;
        }

        if (cfg && cfg.parse) {
          for (let name in cfg.parse) {
            let pelem = element.querySelector("[name='"+name+"']");
            form_object[name] = cfg.parse[name](pelem);
          }
        }
        console.log(form_object);

        if (_this.fform_element) {

          callback(await XHR.post(
            req_path,
            { 
              formData: new FormData(_this.fform_element),
              headers: form_object,
              on_progress: function(e) {
                if (e.lengthComputable) {
                  console.log((e.loaded / e.total) * 100);
                }
              }
            }, true), form_object);
        } else {
          callback(await XHR.post(req_path, form_object), form_object);

        }
      } catch (e) {
        console.error(e.stack);
      }
    });
  }
}
