
const XHR = require("core/utils/xhr_async");

module.exports = class {
  static async construct(req_path, req_json, construct, cfg) {
    try {
      let list = await XHR.post(req_path, req_json);
      console.log(name, "; list:", list);
      console.log(req_json);
      let _this = new module.exports(cfg, list, construct);
      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(cfg, rows, construct) {
    this.element = document.createElement("div");

    if (!cfg) cfg = {};

    this.rows = rows;

    for (let r = 0; r < rows.length; r++) {
      let radio_element = document.createElement(cfg.links ? "a" : "div");
      let row = rows[r];
      


      construct(row, radio_element);

      this.element.appendChild(radio_element);
    }

  }
}

