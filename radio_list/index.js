

module.exports = class {

  constructor(name, rows, construct) {
    this.element = document.createElement("div");

    for (let r = 0; r < rows.length; r++) {
      let radio_element = document.createElement("div");
      let row = rows[r];
      
      let radio = document.createElement("input");
      radio.type = "radio";
      radio.name = name;

      construct(row, radio, radio_element);

      radio_element.appendChild(radio);
      this.element.appendChild(radio_element);
    }

  }
}
