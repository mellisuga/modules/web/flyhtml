

module.exports = class {
  static Form = require("./form/index.js");
  static StepForm = require("./step_form/index.js");
  static RadioList = require("./radio_list/index.js");
  static List = require("./list/index.js");
}
